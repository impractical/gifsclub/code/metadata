module gifs.club/code/metadata

require (
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-memdb v0.0.0-20180223233045-1289e7fffe71
	github.com/pkg/errors v0.8.0
)
